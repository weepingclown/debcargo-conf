Index: rust-tower-http-0.4.4/src/request_id.rs
===================================================================
--- rust-tower-http-0.4.4.orig/src/request_id.rs
+++ rust-tower-http-0.4.4/src/request_id.rs
@@ -486,10 +486,12 @@ mod tests {
     use std::{
         convert::Infallible,
         sync::{
-            atomic::{AtomicU64, Ordering},
+            atomic::Ordering,
             Arc,
         },
     };
+    #[cfg(target_has_atomic = "64")]
+    use std::sync::atomic::AtomicU64;
     use tower::{ServiceBuilder, ServiceExt};
 
     #[allow(unused_imports)]
@@ -497,6 +499,7 @@ mod tests {
 
     #[tokio::test]
     #[cfg(feature = "util")]
+    #[cfg(target_has_atomic = "64")]
     async fn basic() {
         let svc = ServiceBuilder::new()
             .set_x_request_id(Counter::default())
@@ -527,6 +530,7 @@ mod tests {
     }
 
     #[tokio::test]
+    #[cfg(target_has_atomic = "64")]
     async fn other_middleware_setting_request_id() {
         let svc = ServiceBuilder::new()
             .override_request_header(
@@ -555,6 +559,7 @@ mod tests {
     }
 
     #[tokio::test]
+    #[cfg(target_has_atomic = "64")]
     async fn other_middleware_setting_request_id_on_response() {
         let svc = ServiceBuilder::new()
             .set_x_request_id(Counter::default())
@@ -575,8 +580,10 @@ mod tests {
     }
 
     #[derive(Clone, Default)]
+    #[cfg(target_has_atomic = "64")]
     struct Counter(Arc<AtomicU64>);
 
+    #[cfg(target_has_atomic = "64")]
     impl MakeRequestId for Counter {
         fn make_request_id<B>(&mut self, _request: &Request<B>) -> Option<RequestId> {
             let id =
