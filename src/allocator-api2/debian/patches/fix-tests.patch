Index: allocator-api2/src/stable/vec/mod.rs
===================================================================
--- allocator-api2.orig/src/stable/vec/mod.rs
+++ allocator-api2/src/stable/vec/mod.rs
@@ -539,14 +539,6 @@ impl<T, A: Allocator> Vec<T, A> {
     ///
     /// The vector will not allocate until elements are pushed onto it.
     ///
-    /// # Examples
-    ///
-    /// ```
-    /// use std::alloc::System;
-    ///
-    /// # #[allow(unused_mut)]
-    /// let mut vec: Vec<i32, _> = Vec::new_in(System);
-    /// ```
     #[inline(always)]
     pub const fn new_in(alloc: A) -> Self {
         Vec {
@@ -580,34 +572,6 @@ impl<T, A: Allocator> Vec<T, A> {
     ///
     /// Panics if the new capacity exceeds `isize::MAX` bytes.
     ///
-    /// # Examples
-    ///
-    /// ```
-    /// use std::alloc::System;
-    ///
-    /// let mut vec = Vec::with_capacity_in(10, System);
-    ///
-    /// // The vector contains no items, even though it has capacity for more
-    /// assert_eq!(vec.len(), 0);
-    /// assert_eq!(vec.capacity(), 10);
-    ///
-    /// // These are all done without reallocating...
-    /// for i in 0..10 {
-    ///     vec.push(i);
-    /// }
-    /// assert_eq!(vec.len(), 10);
-    /// assert_eq!(vec.capacity(), 10);
-    ///
-    /// // ...but this may make the vector reallocate
-    /// vec.push(11);
-    /// assert_eq!(vec.len(), 11);
-    /// assert!(vec.capacity() >= 11);
-    ///
-    /// // A vector of a zero-sized type will always over-allocate, since no
-    /// // allocation is necessary
-    /// let vec_units = Vec::<(), System>::with_capacity_in(10, System);
-    /// assert_eq!(vec_units.capacity(), usize::MAX);
-    /// ```
     #[cfg(not(no_global_oom_handling))]
     #[inline(always)]
     pub fn with_capacity_in(capacity: usize, alloc: A) -> Self {
@@ -1815,6 +1779,7 @@ impl<T, A: Allocator> Vec<T, A> {
     /// #![feature(vec_push_within_capacity)]
     ///
     /// use std::collections::TryReserveError;
+    /// use std::iter::FromIterator;
     /// fn from_iter_fallible<T>(iter: impl Iterator<Item=T>) -> Result<Vec<T>, TryReserveError> {
     ///     let mut vec = Vec::new();
     ///     for value in iter {
@@ -2995,12 +2960,6 @@ impl<T, A: Allocator, const N: usize> Fr
     /// Convert a boxed array into a vector by transferring ownership of
     /// the existing heap allocation.
     ///
-    /// # Examples
-    ///
-    /// ```
-    /// let b: Box<[i32; 3]> = Box::new([1, 2, 3]);
-    /// assert_eq!(Vec::from(b), vec![1, 2, 3]);
-    /// ```
     #[inline(always)]
     fn from(s: Box<[T; N], A>) -> Self {
         s.into_vec()
@@ -3058,12 +3017,14 @@ impl<T, A: Allocator, const N: usize> Tr
     /// # Examples
     ///
     /// ```
+    /// use std::convert::TryInto;
     /// assert_eq!(vec![1, 2, 3].try_into(), Ok([1, 2, 3]));
     /// assert_eq!(<Vec<i32>>::new().try_into(), Ok([]));
     /// ```
     ///
     /// If the length doesn't match, the input comes back in `Err`:
     /// ```
+    /// use std::convert::TryInto;
     /// let r: Result<[i32; 4], _> = (0..10).collect::<Vec<_>>().try_into();
     /// assert_eq!(r, Err(vec![0, 1, 2, 3, 4, 5, 6, 7, 8, 9]));
     /// ```
@@ -3071,6 +3032,7 @@ impl<T, A: Allocator, const N: usize> Tr
     /// If you're fine with just getting a prefix of the `Vec<T>`,
     /// you can call [`.truncate(N)`](Vec::truncate) first.
     /// ```
+    /// use std::convert::TryInto;
     /// let mut v = String::from("hello world").into_bytes();
     /// v.sort();
     /// v.truncate(2);
